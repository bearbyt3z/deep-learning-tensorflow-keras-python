# TensorFlow examples for Artificial Neural Networks course

See my course webpage for more details on the subject (only in Polish): [Sztuczne sieci neuronowe, Laboratorium (30 godz.),
Informatyka Stosowana, II stopień](http://fizyka.umk.pl/~kdobosz/nn/)

## Table of contents

* [Simple MLP model with softmax output layer](#Simple-MLP-model-with-softmax-output-layer)
* [MLP and CNN model for MNIST dataset](#MLP-and-CNN-model-for-MNIST-dataset)
* [CNN model for CIFAR-10 dataset with data augmentation](#CNN-model-for-CIFAR-10-dataset-with-data-augmentation)
* [LSTM model with CNN layer for IMDB sentiment classification task](#LSTM-model-with-CNN-layer-for-IMDB-sentiment-classification-task)
* [References](#References)

## Simple MLP model with softmax output layer

[Multilayer Perceptron (MLP)](https://en.wikipedia.org/wiki/Multilayer_perceptron) with softmax output layer applied to simple *Iris* dataset.

Dataset: [Fisher's *Iris* dataset](https://en.wikipedia.org/wiki/Iris_flower_data_set)

Jupyter notebook: [`keras_iris_softmax.ipynb`](keras_iris_softmax.ipynb)

Python code: [`keras_iris_softmax.py`](keras_iris_softmax.py)

Google Colab link: https://colab.research.google.com/drive/1aKdMT4E9LiSm-kQn7Zq563oSP6t1VHxb?usp=sharing


## MLP and CNN model for MNIST dataset

Multilayer perceptron vs [Convolutional Neural Network (CNN)](https://en.wikipedia.org/wiki/Convolutional_neural_network) model applied to MNIST dataset. Confusion matrix plotted with the [`pretty-print-confusion-matrix`](https://github.com/wcipriano/pretty-print-confusion-matrix) package by [Wagner Cipriano](https://github.com/wcipriano) (with some modifications of depracted features).

Dataset: [MNIST database](https://en.wikipedia.org/wiki/MNIST_database)

Jupyter notebook: [`keras_mnist_mlp_cnn.ipynb`](keras_mnist_mlp_cnn.ipynb)

Python code: [`keras_mnist_mlp_cnn.py`](keras_mnist_mlp_cnn.py)

Google Colab link: https://colab.research.google.com/drive/10jk-baxSk-H4i2Bxl-f-4IfcX7HUqDVS?usp=sharing


## CNN model for CIFAR-10 dataset with data augmentation

Convolutional neural network model applied to CIFAR-10 dataset with data augmentation option.

Dataset: [CIFAR-10 dataset](https://en.wikipedia.org/wiki/CIFAR-10)

Jupyter notebook: [`keras_cifar10_cnn.ipynb`](keras_cifar10_cnn.ipynb)

Python code: [`keras_cifar10_cnn.py`](keras_cifar10_cnn.py)

Google Colab link: https://colab.research.google.com/drive/1h2H9CB9euVskw95kovt81aDJquTtr5wy?usp=sharing


## LSTM model with CNN layer for IMDB sentiment classification task

[Long Short-Term Memory (LSTM)](https://en.wikipedia.org/wiki/Long_short-term_memory) recurrent neural network model with convolutional layer applied to IMDB sentiment classification task.

Dataset: [IMDB movie review sentiment classification dataset](https://keras.io/api/datasets/imdb/)

Jupyter notebook: [`keras_imdb_lstm_cnn.ipynb`](keras_imdb_lstm_cnn.ipynb)

Python code: [`keras_imdb_lstm_cnn.py`](keras_imdb_lstm_cnn.py)

Google Colab link: https://colab.research.google.com/drive/19E-SmphSZPqjpB9GJJKapakQVAfMkLu1?usp=sharing

Papers comparison: https://paperswithcode.com/sota/sentiment-analysis-on-imdb


## References

1. Keras code examples: https://keras.io/examples
1. Keras datasets: https://keras.io/api/datasets
1. `pretty-print-confusion-matrix` package by Wagner Cipriano: https://github.com/wcipriano/pretty-print-confusion-matrix
